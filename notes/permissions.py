# -*- coding: utf-8 -*-

from rest_framework import permissions


class IsAuthorOfNote(permissions.BasePermission):
    def has_object_permission(self, request, view, note):
        if request.user:
            return note.author == request.user
        return False
