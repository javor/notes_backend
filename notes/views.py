# -*- coding: utf-8 -*-

from rest_framework import permissions, viewsets
from rest_framework.response import Response

from notes.models import Note
from notes.permissions import IsAuthorOfNote
from notes.serializers import NoteSerializer


class NoteViewSet(viewsets.ModelViewSet):
    serializer_class = NoteSerializer

    def get_queryset(self):
        user = self.request.user
        queryset = Note.objects.filter(author=user).order_by('id')
        return queryset

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return permissions.AllowAny(),
        return permissions.IsAuthenticated(), IsAuthorOfNote()

    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)

        return super(NoteViewSet, self).perform_create(serializer)


class AccountNotesViewSet(viewsets.ViewSet):
    queryset = Note.objects.select_related('author').all()
    serializer_class = NoteSerializer

    def list(self, request, account_username=None):
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)
