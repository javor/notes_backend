# -*- coding: utf-8 -*-

from rest_framework import serializers

from authentication.serializers import AccountSerializer
from notes.models import Note


class NoteSerializer(serializers.ModelSerializer):
    author = AccountSerializer(read_only=True, required=False)

    class Meta:
        model = Note

        fields = ('id', 'author', 'content', 'created_at', 'updated_at')
        read_only_fields = ('id', 'created_at', 'updated_at')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(NoteSerializer, self).get_validation_exclusions()

        return exclusions + ['author']
